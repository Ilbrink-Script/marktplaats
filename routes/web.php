<?php

use App\Http\Controllers\AdvertisementController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommissionController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\MessageThreadController;
use App\Http\Controllers\OfferController;
use App\Http\Controllers\StripeController;
use App\Mail\MessageNotification;
use App\Models\Commission;
use App\Models\Message;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;
use Lodge\Postcode\Facades\Postcode;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return Inertia::render('Index');
// })->name('index');

// advertisements
Route::get('/', [AdvertisementController::class, 'list'])->name('advertisements.list');
Route::get('advertisements/{advertisement}', [AdvertisementController::class, 'show'])->name('advertisements.show');
Route::get('search', [AdvertisementController::class, 'search'])->name('advertisements.search');
Route::match(['post', 'get'], 'search/algolia/{searchQuery}', [AdvertisementController::class, 'searchResults'])->name('advertisements.search_result');
Route::match(['post', 'get'], 'search/zipcode/{searchZipcode}/{distance?}', [AdvertisementController::class, 'searchDistanceResults'])->name('advertisements.search_distance_result');

// categories
Route::get('categories', [CategoryController::class, 'index'])->name('categories.index');
Route::get('categories/{category:slug}', [CategoryController::class, 'show'])->name('categories.show');

// stripe webhook, maybe somehow restrict access to stripe.com / stripe-cli whatever? also check which methods are needed
Route::any('webhook', [StripeController::class, 'webhook'])->name('stripe.webhook'); // also add except in VerifyCsrfToken!

// test page | during development only
Route::get('test', function (Request $request) {
    dd($request);
    // $postcode = Postcode::lookup('9718');
    // dd(App::environment('local'));
    // return $postcode;

    // return Inertia::render('Test');
    
    // 'timezone' => Carbon::now(config('app.timezone', date_default_timezone_get()))->timezone->toOffsetName(),
    // return Carbon::now(config('app.timezone', date_default_timezone_get()))->timezone->toOffsetName();
    $msg = Commission::find(1);


    // $msg->paid_at = now(); //  Carbon::now(); // Carbon::now(date_default_timezone_get());
    // $msg->save();

    return $msg;
});

Route::get('tmp', function() {
    // return $file;

    /*
    return view('tmp', [
        'stripe_key' =>  env('STRIPE_KEY'),
        'client_secret' => Commission::find(6)->createOrGetPaymentIntent()->client_secret,
    ]);
    */
});

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    // offers
    Route::post('advertisements/{advertisement}/offer', [OfferController::class, 'store'])->name('offers.store');
    Route::put('advertisements/{advertisement}/accept/{offer}', [AdvertisementController::class, 'accept'])->name('offers.accept');
    Route::put('advertisements/{advertisement}/cancel/{offer}', [AdvertisementController::class, 'cancel'])->name('offers.cancel');

    // messages
    Route::get('messages', [MessageThreadController::class, 'index'])->name('messages.index');
    Route::get('messages/{messageThread}', [MessageThreadController::class, 'show'])->name('messages.show');
    Route::post('advertisements/{advertisement}/message', [MessageThreadController::class, 'store'])->name('message_threads.store');
    Route::post('messages/{messageThread}', [MessageController::class, 'store'])->name('messages.store');

    // advertisements back-end
    Route::get('advertisements', [AdvertisementController::class, 'index'])->name('advertisements.index');
    Route::post('advertisements', [AdvertisementController::class, 'store'])->name('advertisements.store');
    Route::put('advertisements/{advertisement}', [AdvertisementController::class, 'update'])->name('advertisements.update');
    Route::delete('advertisements/{advertisement}', [AdvertisementController::class, 'destroy'])->name('advertisements.destroy');

    // payment stuff
    Route::get('advertisement/{advertisement}/premium', [AdvertisementController::class, 'premium'])->name('advertisements.premium');
    Route::get('advertisement/{advertisement}/process', [AdvertisementController::class, 'process'])->name('advertisements.process');

    Route::get('commissions', [CommissionController::class, 'index'])->name('commissions.index');
    Route::get('commissions/{commission}', [CommissionController::class, 'show'])->name('commissions.show');
    Route::post('commissions/{commission}/session', [CommissionController::class, 'session'])->name('commissions.session');
    Route::get('commissions/{commission}/process', [CommissionController::class, 'process'])->name('commissions.process');



    // redirect to stripe
    Route::get('/billing-portal', function (Request $request) {
        return $request->user()->redirectToBillingPortal(route('commissions.index'));
    });

    // dashboard, maybe do something with this, or throw away...
    Route::get('dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

    // mailable preview
    Route::get('mailable', function () {
        $message = Message::whereNull('notified_at')->latest()->get()->first();

        if (!$message) {
            return "No message for mailable found";
        }

        return new MessageNotification($message);
    });
});

// Route::get('/welcome', function () {
//     return view('welcome');
// })->name('welcome');
