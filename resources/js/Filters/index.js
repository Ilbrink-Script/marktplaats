import Vue from "vue";

Vue.filter('fmtDate', val => moment(val).format('YYYY-MM-DD'));
Vue.filter('fmtTime', val => moment(val).format('HH:mm'));
Vue.filter('fmtDateTime', val => moment(val).format('YYYY-MM-DD HH:mm'));
Vue.filter('fmtDateTimeFancy', val => {
    const valMom = moment(val);
    const now = moment();

    if (valMom.isSame(now, 'minute')) return now.diff(valMom, 'seconds') + ' seconds ago';
    if (valMom.isSame(now, 'day')) return Vue.filter('fmtTime')(val);
    if (valMom.isSame(now, 'week')) return valMom.format('dddd HH:mm');
    if (valMom.isSame(now, 'year')) return valMom.format('D MMMM HH:mm');

    return Vue.filter('fmtDateTime')(val);
});
Vue.filter('fmtPrice', val => '€ ' + Number(val).toFixed(2));
