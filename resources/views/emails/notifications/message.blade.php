@php
$message = $messages->first();
$messageThread = $message->messageThread;
@endphp

@component('mail::message')
# You have a new message

Hello {{ $message->recipient->name}},

You received @if ($messages->count() > 1)
    new messages
@else
    a new message
@endif from _{{ $message->user->name }}_<br />
related to advertisement _{{ trim($messageThread->advertisement->title) }}_

@foreach ($messages as $msg)
## {{ trim($msg->title) }}
@foreach (explode("\n", $msg->body) as $line)
> {{ $line }}
@endforeach
@if (!$loop->last)
---
@endif
@endforeach

@component('mail::button', ['url' => route('messages.show', $messageThread)])
Read the entire conversation
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
