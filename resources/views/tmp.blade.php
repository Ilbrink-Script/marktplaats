<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Stripe test</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</head>

<body>
    <h1>Let's do some stripe payments</h1>
    secret: {{ $client_secret }}<br />
    stripe_key: {{ $stripe_key }}<br />
    <ul class="nav nav-tabs" id="stripeTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="card-tab" data-bs-toggle="tab" href="#card" role="tab" aria-controls="card" aria-selected="true">Card</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="ideal-tab" data-bs-toggle="tab" href="#ideal" role="tab" aria-controls="ideal" aria-selected="false">iDEAL</a>
        </li>
    </ul>
    <div class="tab-content" id="stripeTabContent" :class="{ 'is-invalid': stripeError }">
        <div class="tab-pane fade show active p-2" id="card" role="tabpanel" aria-labelledby="card-tab">
            <!-- <stripe-element-card
                                        ref="cardRef"
                                        :pk="stripe_key"
                                        :hidePostalCode="hidePostalCode"
                                        @token="tokenCreated"
                                        @element-change="cardChange"
                                    /> -->
            <div id="card-element" class="p-2">
                <!-- A Stripe card Element will be inserted here. -->
            </div>
        </div>
        <div class="tab-pane fade p-2" id="ideal" role="tabpanel" aria-labelledby="ideal-tab">
            <div id="ideal-bank-element" class="p-2">
                <!-- A Stripe iDEAL Element will be inserted here. -->
            </div>
        </div>
    </div>


    <div id="input-error" class="invalid-feedback my-2" role="alert">
        <strong></strong>
    </div>
    <button class="mr-2 btn btn-primary" onclick="makePayment()">Pay</button>
    <script>
        const STRIPE_JS_SDK_URL = "https://js.stripe.com";
        const stripe_key = "{{ $stripe_key }}";
        const client_secret = "{{ $client_secret }}";
        let stripe;
        let stripeError;
        let card;
        let idealBank;

        const makePayment = function() {
            // e.preventDefault();

            console.log("lets make a payment");

            const paymentMethod = document.querySelector("#stripeTab a.active");
            switch (paymentMethod.getAttribute("aria-controls")) {
                case "ideal":
                    payWithIdeal();
                    break;
                case "card":
                    payWithCard();
                    break;
                default:
                    console.error("No payment method found");
            }

            // this.payWithCard(stripe, card, client_secret);
        };
        const payWithCard = function() {
            // loading(true); // exists?
            stripe
                .confirmCardPayment(client_secret, {
                    payment_method: {
                        card: card
                    }
                })
                .then(result => {
                    if (result.error) {
                        // Show error to your customer
                        console.error(result.error);
                        stripeError = result.error.message;
                    } else {
                        // The payment succeeded! Do stuff to handle the commission thing.
                        console.log("success CARD", result);
                    }
                });
        };
        const payWithIdeal = function() {
            // Initiate the payment.
            // confirmIdealPayment will redirect the customer to their bank

            stripe
                .confirmIdealPayment(
                    client_secret, {
                        payment_method: {
                            ideal: ideal
                        },
                        return_url: `${window.location.href}/complete`
                    }
                )
                .then(result => {
                    if (result.error) {
                        // Show error to your customer
                        console.error(result.error);
                        stripeError = result.error.message;
                    } else {
                        // The payment has been processed!
                        console.log("success iDEAL", result);
                        // orderComplete(clientSecret);
                    }
                });
        };

        const loadStripeSdk = (version = "v3", callback) => {
            if (window.Stripe) {
                callback();
                return;
            }
            let e = document.createElement("script");
            e.src = `${STRIPE_JS_SDK_URL}/${version}`;
            e.type = "text/javascript";
            document.getElementsByTagName("head")[0].appendChild(e);
            e.addEventListener("load", callback);
        };

        const setupStripe = () => {
            stripe = Stripe(stripe_key, {
                betas: ["ideal_pm_beta_1"]
            });

            const elements = stripe.elements();

            card = elements.create("card");
            card.mount("#card-element");

            idealBank = elements.create("idealBank");
            idealBank.mount("#ideal-bank-element");
        };

        loadStripeSdk("v3", setupStripe);
    </script>
</body>

</html>