<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdvertisementFormRequest;
use App\Models\Advertisement;
use App\Models\AdvertisementStatus;
use App\Models\Category;
use App\Models\Commission;
use App\Models\Offer;
use App\Models\Pp4;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;
use PhpParser\Node\Expr\Cast\Double;
use Stripe\PaymentIntent;
use Stripe\Service\PaymentIntentService;

class AdvertisementController extends Controller
{
    protected $pageSize = 10;
    protected $advertisementData = ['user', 'offers.user', 'categories', 'advertisementStatus', 'commission'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $advertisementStatuses = AdvertisementStatus::all();
        $categories = Category::all();

        $advertisements = Auth::user()
            ->advertisements()
            ->orderBy('updated_at', 'desc')
            ->with(['advertisementStatus', 'categories'])
            ->get();

        return Inertia::render('Advertisement/Index', compact([
            'advertisements',
            'advertisementStatuses',
            'categories'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdvertisementFormRequest
     * @return \Illuminate\Http\Response
     */
    public function store(AdvertisementFormRequest $request)
    {
        $validatedData = $request->validated();

        $advertisement = Advertisement::make($validatedData);
        $advertisement->user()->associate(Auth::user());
        $advertisement->save();


        if ($request->has('categories')) {
            $advertisement->categories()->attach($validatedData['categories']);
        }

        return Redirect::route("advertisements.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function show(Advertisement $advertisement)
    {
        $advertisement = $this->getDistanceBuilder(Advertisement::where('id', $advertisement->id))->first();
        // dd($advertisement);
        $advertisement->loadMissing($this->advertisementData);


        return Inertia::render('Advertisement/Show', compact([
            'advertisement',
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertisement $advertisement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AdvertisementFormRequest
     * @param  \App\Models\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function update(AdvertisementFormRequest $request, Advertisement $advertisement)
    {
        $validatedData = $request->validated();

        $advertisement->update($validatedData);

        if ($request->has('categories')) {
            $advertisement->categories()->sync($validatedData['categories']); // update categories
        } else {
            $advertisement->categories()->detach();
        }

        return redirect()->route('advertisements.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advertisement $advertisement)
    {
        $advertisement->delete();
        return Redirect::route('advertisements.index');
    }

    /**
     * Display a list of all active advertisements
     */
    public function list()
    {
        $status = AdvertisementStatus::where('slug', 'active')->first();
        $advertisements = $this->getDistanceBuilder($status->advertisements())
            ->orderByDesc('premium')
            ->orderByDesc('premium_at')
            ->orderByDesc('created_at')
            ->paginate($this->pageSize);
        $advertisements->loadMissing($this->advertisementData);

        return Inertia::render('Advertisement/List', compact(['advertisements']));
    }

    /**
     * Display a search page
     */
    public function search()
    {
        $searchDefaults = [
            'zipcode' => Auth::user() ? Auth::user()->zipcode : '9723AC',
            'distance' => 25,
        ];
        return Inertia::render('Search/Index', $searchDefaults);
    }

    /**
     * Display a list of search results advertisements
     */
    public function searchResults(Request $request, String $urlSearchQuery)
    {
        // POST
        if ($request->method === "POST") {
            $validatedData = $request->validate([
                'search_query' => ['required', 'string',],
            ]);
        }
        // GET
        else {
            // ehm...
        }

        $searchQuery = trim(urldecode($urlSearchQuery));

        // dd($request);

        // $advertisements = Advertisement::all()->paginate();
        $advertisements = Advertisement::search($searchQuery)
            ->paginate($this->pageSize);
        $advertisements->loadMissing($this->advertisementData);

        // get rid of that stupid query parameter
        $advertisements->appends('query', null);

        return Inertia::render('Search/Index', compact(['advertisements', 'searchQuery']));
    }

    /**
     * Display a list of search results by distance advertisements
     */
    public function searchDistanceResults(Request $request, String $urlSearchZipcode, $distance = null)
    {
        $validationRules = [
            'search_zipcode' => ['required', 'string', 'regex:/^\d{4}(?:[A-Za-z]{2})?$/i'],
            'distance' => ['nullable', 'integer', 'min:0'],
        ];

        // POST
        if ($request->method() === "POST") {
            $validatedData = $request->validate($validationRules);
        }
        // GET
        else {
            // dd('HUH, this should be post?? ' . $request->method());
            $inputData = [
                'search_zipcode' => urldecode($urlSearchZipcode),
                'distance' => $distance,
            ];
            $validatedData = Validator::make($inputData, $validationRules)->validate();
        }

        // maybe somehow validate on exists in 4pp table?
        $zipcodeNumbers = substr($validatedData['search_zipcode'], 0, 4);
        $postcode = Pp4::where('postcode', $zipcodeNumbers)->first();

        // valid postcode record 
        if ($postcode) {
            if (is_null($validatedData['distance']) || $validatedData['distance'] == 0) {
                $query = Advertisement::distance($postcode->latitude, $postcode->longitude);
            } else {
                $query = Advertisement::geofence($postcode->latitude, $postcode->longitude, 0, $validatedData['distance']);
            }

            $advertisements = $query->orderBy('distance', 'asc')->paginate($this->pageSize);
            $advertisements->loadMissing($this->advertisementData);
        }
        // 
        else {
            $postcodeErrors = [
                'search_zipcode' => ['This dutch zipcode does not exist'],
            ];
            $wrongPostcodeException = ValidationException::withMessages($postcodeErrors);
            throw $wrongPostcodeException;
        }

        return Inertia::render('Search/Index', [
            'advertisements' => $advertisements,
            'distance' => intval($validatedData['distance']),
            'zipcode' => $validatedData['search_zipcode'],
        ]);
    }

    /**
     * Add distance properties to query builder
     *
     * @param  Builder $advertisements
     * @param  Double $latitude
     * @param  Double $longitude
     * @return Builder
     */
    protected function getDistanceBuilder($builder, $latitude = null, $longitude = null)
    {
        if (is_null($latitude) && Auth::check()) {
            $latitude = Auth::user()->latitude;
            $longitude = Auth::user()->longitude;
        }

        if (doubleval($latitude) === 0 && doubleval($longitude) === 0)
            return $builder;


        return $builder->distance($latitude, $longitude);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Advertisement  $advertisement
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function accept(Request $request, Advertisement $advertisement, Offer $offer)
    {

        // make sure all mutations happen or none at all
        DB::transaction(function () use ($advertisement, $offer) {

            // update offer id by defining the offer that's being accepted
            $advertisement->acceptedOffer()->associate($offer);
            // $offer->advertisement()->associate($advertisement);

            // update status
            $status = AdvertisementStatus::where('slug', 'sold')->first();
            $advertisement->advertisementStatus()->associate($status);

            // update the advertisement
            $advertisement->offer_accepted_at = now(); //  Carbon::now();

            $advertisement->save();

            // generate commission request
            if ($offer->price > Commission::$priceThreshold) {
                $commission = Commission::make([
                    'price' => $offer->price * 0.05
                ]);
                $commission->advertisement()->associate($advertisement);
                $commission->offer()->associate($offer);
                $commission->save();

                // do something with stripe? YES! create payment intent
                $commission->createOrGetPaymentIntent();
            }
        });

        return redirect()->route('advertisements.show', $advertisement);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Advertisement  $advertisement
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function cancel(Request $request, Advertisement $advertisement, Offer $offer)
    {
        // update status
        $status = AdvertisementStatus::where('slug', 'active')->first();
        $advertisement->advertisementStatus()->associate($status);

        // update offer id by defining the offer that's being accepted
        $advertisement->offer_id = null;
        // $advertisement->acceptedOffer()->detach($offer);

        // update the advertisement
        $advertisement->offer_accepted_at = null;
        $advertisement->save();

        return redirect()->route('advertisements.show', $advertisement);
    }

    /**
     * Show the form for making a premium payment.
     *
     * @param  \App\Models\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function premium(Advertisement $advertisement)
    {
        $this->authorize('update', $advertisement);

        $amount = 10 * 100;
        $stripe_key = env("STRIPE_KEY");
        $payment_intent_minimized = $advertisement->paymentIntentMinimized;



        return Inertia::render('Advertisement/Premium', compact(['advertisement', 'amount', 'stripe_key', 'payment_intent_minimized']));
    }

    /**
     * Process the premium payment 
     *     
     * @param \Illuminate\Http\Request
     * @param  \App\Models\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function process(Request $request, Advertisement $advertisement)
    {
        $this->authorize('update', $advertisement);


        $validator = Validator::make($request->all(), [
            'payment_intent' => Rule::in($advertisement->payment_intent_id),
            'payment_intent_client_secret' => 'string:required',
            'redirect_status' => 'string:required',
            'source_type' => 'string:required',
        ]);

        if ($validator->fails()) {
            return redirect(route('advertisements.index'));
        }

        $payment_intent_minimized = $advertisement->paymentIntentMinimized;

        return Inertia::render('Advertisement/Process', compact(['advertisement', 'payment_intent_minimized']));
    }
}
