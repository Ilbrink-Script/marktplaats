<?php

namespace App\Http\Controllers;

use App\Http\Requests\OfferFormRequest;
use App\Models\Advertisement;
use App\Models\Offer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Advertisement $advertisement)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  OfferFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Advertisement $advertisement, OfferFormRequest $request)
    {
        $validatedData = $request->validated();

        $offer = Offer::make($validatedData);
        $offer->user()->associate(Auth::user());
        $offer->advertisement()->associate($advertisement);
        $offer->save();

        return Redirect::route('advertisements.show', $advertisement);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        //
    }
}
