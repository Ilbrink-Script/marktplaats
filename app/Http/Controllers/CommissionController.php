<?php

namespace App\Http\Controllers;

use App\Models\Commission;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Stripe\Checkout\Session;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class CommissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commissions = Auth::user()->commissions->load(['advertisement', 'offer']);
        // $stripeCustomer = Auth::user()->createOrGetStripeCustomer(); // "72B308B9"

        // $first = $commissions->first();
        // echo $first->created_at;
        // echo '<hr />';
        // echo $first->paid_at;
        // echo '<hr />';
        // return $first;

        return Inertia::render('Commissions/Index', compact(['commissions',])); //  'stripeCustomer']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function show(Commission $commission)
    {
        $this->authorize('update', $commission);

        $commission->loadMissing(['offer']);
        $stripe_key = env('STRIPE_KEY');
        $stripe_customer = Auth::user()->createOrGetStripeCustomer();
        $payment_intent = $commission->createOrGetPaymentIntent();
        $payment_intent_minimized = $commission->payment_intent_minimized; // Arr::only($payment_intent->toArray(), ['description', 'amount', 'statement_descriptor', 'client_secret']);

        // @TODO retrieve a stored ideal preferred bank in metadata?

        return Inertia::render('Commissions/Show', compact(['commission', 'stripe_key', 'payment_intent_minimized', 'stripe_customer']));
    }

    /**
     * Generate a new Stripe Checkout Session for the provided Commission
     *
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function session(Commission $commission)
    {
        $session = Session::create([
            'success_url' => route('commissions.process', $commission),
            'cancel_url' => route('commissions.process', $commission),
            'payment_intent' => $commission->createOrGetPaymentIntent()->id,
        ]);

        return $session;
    }

    /**
     * Process Stripe Checkout Session return url(s)
     *
     * @return \Illuminate\Http\Request
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function process(Request $request, Commission $commission)
    {
        $this->authorize('update', $commission);
        /*
        http://localhost:8000/commissions/1/process
            ?payment_intent=pi_1IBIkLFmXSeYgfGnf5JFUIPL
            &payment_intent_client_secret=pi_1IBIkLFmXSeYgfGnf5JFUIPL_secret_vp7hExO6ZCFgKjB3yRPErjVe9
            &redirect_status=failed
            &source_type=ideal
        */
        $validator = Validator::make($request->all(), [
            'payment_intent' => Rule::in($commission->payment_intent_id),
            'payment_intent_client_secret' => 'string:required',
            'redirect_status' => 'string:required',
            'source_type' => 'string:required',
        ]);

        if ($validator->fails()) {
            return redirect(route('commissions.index'));
        }


        return Inertia::render('Commissions/Process', [
            'commission' => $commission,
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function edit(Commission $commission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commission $commission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commission $commission)
    {
        //
    }
}
