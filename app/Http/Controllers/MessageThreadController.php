<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageThreadFormRequest;
use App\Models\Advertisement;
use App\Models\Message;
use App\Models\MessageThread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class MessageThreadController extends Controller
{
    protected $threadData = ['user', 'advertisement.user', 'messages.user'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $outgoingThreads = Auth::user()->messageThreads()->with($this->threadData)->get();
        $incomingThreads = Auth::user()->advertisementMessageThreads()->with($this->threadData)->get();

        return Inertia::render('Message/Index', compact(['outgoingThreads', 'incomingThreads']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\MessageThreadFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MessageThreadFormRequest $request, Advertisement $advertisement)
    {
        $data = $request->validated();

        $messageThread = MessageThread::make($data);
        $messageThread->user()->associate(Auth::user());
        $messageThread->advertisement()->associate($advertisement);
        $messageThread->save();

        $message = Message::make($data);
        $message->messageThread()->associate($messageThread);
        $message->user()->associate(Auth::user());
        $message->save();

        return Redirect::route('messages.show', $messageThread);

        // maybe redirect to individual advertisement page or correct pagination etc?
        // return Redirect::route('advertisements.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MessageThread  $messageThread
     * @return \Illuminate\Http\Response
     */
    public function show(MessageThread $messageThread)
    {
        $messageThread->loadMissing($this->threadData);
        return Inertia::render('Message/Show', compact(['messageThread']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MessageThread  $messageThread
     * @return \Illuminate\Http\Response
     */
    public function edit(MessageThread $messageThread)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MessageThread  $messageThread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MessageThread $messageThread)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MessageThread  $messageThread
     * @return \Illuminate\Http\Response
     */
    public function destroy(MessageThread $messageThread)
    {
        //
    }
}
