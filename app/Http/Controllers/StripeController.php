<?php

namespace App\Http\Controllers;

use App\Models\Advertisement;
use App\Models\Commission;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Stripe\Event;
use Stripe\Exception\SignatureVerificationException;
use Stripe\Exception\UnexpectedValueException;
use Stripe\PaymentIntent;
use Stripe\Webhook;

class StripeController extends Controller
{

    /**
     *      * 
     * @param  Request  $request
     */
    public function webhook(Request $request)
    {
        // Set your secret key. Remember to switch to your live secret key in production!
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        // \Stripe\Stripe::setApiKey('sk_test_51I1tLqFmXSeYgfGn9bzATpgMPOCHSm4fLgtxaR20YQwxU4AMm5VTsdAIG5IikIyPm9n3B1pCogKwTaUw06PQmBM500LtsV7y9e');

        $endpoint_secret = env('STRIPE_WEBHOOK_SECRET');

        // dd($_SERVER);

        $payload = @file_get_contents('php://input');
        // $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $sig_header = $request->header("stripe-signature");

        // $sig_header = $request->header('http-stripe-signature');
        $event = null;

        $logFile = 'webhook.log';
        $log = Storage::disk('local');

        $log->prepend($logFile, $payload);
        // $log->prepend($logFile, 'sig_header2: ' . $sig_header2);
        // $log->prepend($logFile, '$_SERVER: ' . print_r($request->headers, true));
        // $log->prepend($logFile, 'sig_header_laravel: ' . $sig_header_laravel);
        // $log->prepend($logFile, 'sig_header_filter: ' . $sig_header_filter);
        // $log->prepend($logFile, 'sig_header: ' . $sig_header);


        try {
            // $event = Event::constructFrom(
            //     json_decode($payload, true)
            // );
            $event = Webhook::constructEvent($payload, $sig_header, $endpoint_secret);
        } catch (UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        } catch (SignatureVerificationException $e) {
            // Invalid signature
            // $log->prepend($logFile, $e);
            echo '<h1>400 - Signature Verification Failed</h1>';
            // echo '<br />$sig_header: [' . $sig_header . ']';
            // $log->prepend($logFile, 'SignatureVerificationException');
            // echo $e;
            http_response_code(400);
            exit();
        }

        // Handle the event
        switch ($event->type) {
            case 'payment_intent.succeeded':
                $paymentIntent = $event->data->object; // contains a StripePaymentIntent
                $this->handlePaymentIntentSucceeded($paymentIntent);
                break;
            case 'payment_method.attached':
                $paymentMethod = $event->data->object; // contains a StripePaymentMethod
                // handlePaymentMethodAttached($paymentMethod);
                echo 'handlePaymentMethodAttached($paymentMethod);';
                break;
                // ... handle other event types
            default:
                echo 'Received unknown event type ' . $event->type;
        }

        http_response_code(200);

        // return $request;
    }

    /**
     * @param PaymentIntent $paymentIntent
     */
    protected function handlePaymentIntentSucceeded(PaymentIntent $paymentIntent)
    {
        $commission = Commission::where('payment_intent_id', $paymentIntent->id)->first();
        $advertisement = Advertisement::where('payment_intent_id', $paymentIntent->id)->first();
        // $commission = Commission::find(1);
        if ($commission) {
            $commission->paid = true;
            $commission->paid_at = now(); // Carbon::now();
            // $commission->paid_at = new DateTime('2021-01-14T12:53:31.000000Z'); // Carbon::now(config('app.timezone', date_default_timezone_get())); // ->timezone->toOffsetName();
            $commission->save();
            echo 'Payment intent succeeded for commission ' . $commission->id;
        } elseif ($advertisement) {
            $advertisement->premium = true;
            $advertisement->premium_at = now();
            $advertisement->save();
            echo 'Payment intent succeeded for advertisement ' . $advertisement->id;
        } else {
            echo 'No commission or advertisement found for payment_intent_id ' . $paymentIntent->id;
        }
    }
}
