<?php

namespace App\Http\Responses;

use Illuminate\Support\Facades\Auth;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;

class LoginResponse implements LoginResponseContract
{
    public function toResponse($request)
    {
        // custom login response
        $roleBasedRedirectRoute = Auth::user()->isSeller ? route('advertisements.list') : route('advertisements.index');

        return $request->wantsJson()
            ? response()->json(['two_factor' => false])
            : redirect()->intended($roleBasedRedirectRoute);
    }
}
