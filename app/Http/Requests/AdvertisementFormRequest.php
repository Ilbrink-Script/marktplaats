<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AdvertisementFormRequest extends FormRequest
{

    protected $errorBag = 'advertisementForm';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() && $this->canEdit();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'advertisement_status_id' => ['required', 'numeric', 'exists:advertisement_statuses,id',],
            'title' => ['required', 'string', 'min:5',],
            'body' => ['required', 'string', 'min:10',],
            'price' => ['required', 'numeric', 'gte:0'],
            'zipcode' => ['required', 'regex:/\d{4}[A-Za-z]{2}/i'],
            'categories' => ['filled', 'exists:categories,id'],
        ];
    }

    protected function canEdit()
    {
        return !$this->advertisement || $this->advertisement->user->is(Auth::user()) || Auth::user()->isAdmin;
    }
}
