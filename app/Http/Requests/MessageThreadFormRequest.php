<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;

class MessageThreadFormRequest extends MessageFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() && !$this->isSeller();
    }

    /**
     * Check if user is the advertisement seller
     */
    public function isSeller()
    {
        return $this->advertisement->user()->is(Auth::user());
    }
}
