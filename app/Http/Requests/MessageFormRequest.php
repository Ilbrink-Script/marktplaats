<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MessageFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() && $this->isParticipant();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string', 'min:5',],
            'body' => ['required', 'string', 'min:10',],
        ];
    }

    public function isParticipant()
    {
        return $this->user()->is(Auth::user()) || $this->advertisement->user()->is(Auth::user());
    }
}
