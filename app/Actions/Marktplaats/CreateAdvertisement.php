<?php

namespace App\Actions\Marktplaats;

use App\Models\Advertisement;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CreateAdvertisement
{

    /**
     * Validate and create a new advertisement.
     *
     * @param  array  $input
     * @return \App\Models\Advertisement
     */
    public function create(User $user, array $input)
    {
        Validator::make($input, [
            'title' => ['required', 'string'],
            'body' => ['required', 'text',],
            'zipcode' => ['required', 'regex:/\d{4}[A-Za-z]{2}/i'],
        ])->validate();

        // dd($input);

        $advertisement = Advertisement::make([
            'name' => $input['title'],
            'body' => $input['body'],
            'zipcode' =>  Str::upper($input['zipcode']),
        ]);
        $advertisement->user()->associate($user);
        $advertisement->save();

        return $advertisement;
    }
}
