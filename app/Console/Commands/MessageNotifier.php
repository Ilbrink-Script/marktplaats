<?php

namespace App\Console\Commands;

use App\Mail\MessageNotification;
use App\Models\Message;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class MessageNotifier extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:message-notifier';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends email notifications for new messages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $messages = Message::whereNull('notified_at')->oldest()->get();

        if ($messages->count() === 0) {
            $this->info('No new messages found');
            return 1;
        }

        $alreadyNotified = [];
        $messages->each(function ($message, $key) use ($messages, &$alreadyNotified) {
            $threadUserKey = "{$message->message_thread_id}|{$message->recipient->id}";
            // $this->error("$threadUserKey start, thread: {$message->message_thread_id}, user {$message->recipient->id}");

            if (in_array($threadUserKey, $alreadyNotified)) {
                $this->info("Skipping message {$message->id} as it was notified already. [$message->notified_at]");
                return;
            }

            $threadUserMessages = $messages->filter(function ($msg) use ($message) {
                return $msg->message_thread_id === $message->message_thread_id
                    && $msg->recipient->id === $message->recipient->id;
            });

            Mail::to($message->recipient)->send(new MessageNotification($threadUserMessages));
            $this->info("Message notification {$message->id} sent to {$message->recipient->name}");

            // update notified_at + older messages for the same thread + user as well
            Message::where([
                ['message_thread_id', $message->message_thread_id],
                ['user_id', $message->user_id],
            ])->whereNull('notified_at')->update([
                'notified_at' => now() // Carbon::now()
            ]);

            // keep track of which thread-user combo has been dealt with already
            array_push($alreadyNotified, $threadUserKey);
        });

        return 0;
    }
}
