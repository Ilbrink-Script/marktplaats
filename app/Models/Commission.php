<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Stripe\PaymentIntent;
use Stripe\StripeClient;

class Commission extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['price'];
    protected $casts = [
        'paid_at' => 'datetime', // if i want the same format as the default created_at / updated_at timestamps
    ];
    protected $appends = [
        'payment_intent_minimized'
    ];

    // protected $appends = ['offer'];
    public static $priceThreshold = 500;

    /**
     * Return the advertisement this commission belongs to
     * 
     * @return Advertisement
     */
    public function advertisement()
    {
        return $this->belongsTo(Advertisement::class);
    }

    /**
     * Return the offer this commission belongs to
     * 
     * @return Offer
     */
    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    /**
     * Return the transaction this commission belongs to
     * 
     * @return Transaction
     */
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    /**
     * Return the price in Stripe format
     */
    public function getStripePriceAttribute()
    {
        return round($this->price * 100, 0, PHP_ROUND_HALF_UP);
    }

    /**
     * Creates or gets the payment intent for the commission
     * 
     * @return PaymentIntent the payment intent for this Commission
     */
    public function createOrGetPaymentIntent($forceUpdate = false)
    {

        // retrieve the existing payment intent
        if (!is_null($this->payment_intent_id)) {
            return PaymentIntent::retrieve($this->payment_intent_id);
        }

        // create a new payment intent
        $stripe_user = $this->advertisement->user->createOrGetStripeCustomer();
        $appName = env('APP_NAME');

        $intent = PaymentIntent::create([
            'amount' => $this->stripePrice,
            'currency' => env('CASHIER_CURRENCY'),
            'customer' => $stripe_user->id,
            'description' => "$appName commission payment for advertisement '{$this->advertisement->title}'",
            'statement_descriptor' => "MP advertisement {$this->advertisement_id}",
            'metadata' => [
                'advertisement_id' => $this->advertisement->id,
                'user_id' => $this->advertisement->user_id,
                'offer_id' => $this->offer_id,
            ],
            'payment_method_types' => [
                'card',
                'ideal',
            ],
            'receipt_email' => $this->advertisement->user->email,
        ]);

        $this->payment_intent_id = $intent->id;
        $this->save();

        return $intent;
    }

    public function getPaymentIntentMinimizedAttribute()
    {
        return  Arr::only($this->createOrGetPaymentIntent()->toArray(), ['description', 'amount', 'statement_descriptor', 'client_secret', 'status']);
    }
}
