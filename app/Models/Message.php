<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'body'];

    /**
     * Return the user that wrote this message
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * Return the message thread this message belongs to
     *
     * @return MessageThread
     */
    public function messageThread()
    {
        return $this->belongsTo(MessageThread::class);
    }

    public function getRecipientAttribute()
    {
        if ($this->messageThread->user->is($this->user))
            return $this->messageThread->advertisement->user;

        return $this->messageThread->user;
    }
}
