<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Laravel\Scout\Searchable;
use Malhal\Geographical\Geographical;
use Stripe\PaymentIntent;

class Advertisement extends Model
{
    use HasFactory;
    use Sluggable;
    use SoftDeletes;
    use Searchable;
    use Geographical;

    protected $minimumOverBid = 0.05;

    protected $appends = ['minimum_offer', 'distance_km', 'best_offer_price',]; //  'payment_intent_minimized'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'advertisement_status_id',
        'title',
        'body',
        'price',
        'zipcode',
    ];

    protected static $kilometers = true;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Return the user that this advertisement belongs to
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Return the advertisement status
     *
     * @return AdvertisementStatus
     */
    public function advertisementStatus()
    {
        return $this->belongsTo(AdvertisementStatus::class);
    }

    /**
     * Return the offers made for this advertisement
     *
     * @return Offer
     */
    public function offers()
    {
        return $this->hasMany(Offer::class)->orderByDesc('price');
    }

    /**
     * Return the minimum offer value that can be made
     *
     * @return Decimal
     */
    public function getMinimumOfferAttribute()
    {
        if (count($this->offers) === 0) return $this->minimumOverBid;

        return $this->offers()->first()->price + $this->minimumOverBid;
    }

    /**
     * Return the message threads for this advertisement
     *
     * @return Collection<MessageThread>
     */
    public function messageThreads()
    {
        return $this->hasMany(MessageThread::class);
    }

    /**
     * Return the linked categories
     *
     * @return Collection<Category>
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        // maybe use a restricted set of searchable properties? Now configured in Algolia dashboard
        $searchArray = ['title', 'body', 'price', 'zipcode'];

        return $array;
    }

    public function shouldBeSearchable()
    {
        return $this->advertisementStatus->slug === 'active';
    }

    /**
     * Return the distance in kilometers 
     *
     * @return 
     */
    public function getDistanceKmAttribute()
    {
        if (!is_double($this->distance))
            return '?';

        if (self::$kilometers) return round($this->distance, 1);

        return round($this->distance * 1.609344, 1);
    }

    public function getBestOfferPriceAttribute()
    {
        if (!$this->offers->count()) return '-';

        return $this->offers->first()->price;
    }


    public function acceptedOffer()
    {
        // return $this->hasOne(Offer::class, 'advertisement_id', 'offer_id');
        return $this->belongsTo(Offer::class, 'offer_id');
    }

    /**
     * Return the commision that belongs to this advertisement
     * 
     * @return Commission
     */
    public function commission()
    {
        return $this->hasOne(Commission::class);
    }

    /**
     * Creates or gets the payment intent for the commission
     * 
     * @return PaymentIntent the payment intent for this Commission
     */
    public function createOrGetPaymentIntent($forceUpdate = false)
    {

        // retrieve the existing payment intent
        if (!is_null($this->payment_intent_id)) {
            return PaymentIntent::retrieve($this->payment_intent_id);
        }

        // create a new payment intent
        $stripe_user = $this->user->createOrGetStripeCustomer();
        $appName = env('APP_NAME');

        $intent = PaymentIntent::create([
            'amount' => 10 * 100,
            'currency' => env('CASHIER_CURRENCY'),
            'customer' => $stripe_user->id,
            'description' => "$appName premium payment for advertisement '{$this->title}'",
            'statement_descriptor' => "MP premium ad {$this->id}",
            'metadata' => [
                'advertisement_id' => $this->id,
                'user_id' => $this->user_id,
            ],
            'payment_method_types' => [
                'card',
                'ideal',
            ],
            'receipt_email' => $this->user->email,
        ]);

        $this->payment_intent_id = $intent->id;
        $this->save();

        return $intent;
    }


    public function getPaymentIntentMinimizedAttribute()
    {
        return  Arr::only($this->createOrGetPaymentIntent()->toArray(), ['description', 'amount', 'statement_descriptor', 'client_secret', 'status']);
    }
}
