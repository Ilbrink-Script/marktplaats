<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MessageThread extends Model
{
    use HasFactory;

    /**
     * Mass assignable attributes
     *
     */
    protected $fillable = ['title'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'last_message',
        'last_reply',
    ];


    /**
     * Return the user that this message thread belongs to
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Return the advertisement this message thread belongs to
     *
     * @return Advertisement
     *
     */
    public function advertisement()
    {
        return $this->belongsTo(Advertisement::class);
    }

    /**
     * Return all messages in this message thread
     *
     * @return Collection<Message>
     *
     */
    public function messages()
    {
        return $this->hasMany(Message::class)->orderBy('created_at');
    }

    /**
     * Return the last message in this thread
     *
     * @return Message
     */
    public function getLastMessageAttribute()
    {
        return  $this->messages->last();;
    }

    /**
     * Return the last reply of the other user in this message thread
     *
     * @return Message or false
     */
    public function getLastReplyAttribute()
    {
        $lastReply = $this->messages->last(function ($message) {
            return $message->user_id !== Auth::user()->id;
        });

        if ($lastReply) return $lastReply;

        return false;
    }
}
