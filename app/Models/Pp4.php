<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pp4 extends Model
{
    use HasFactory;

    protected $table = '4pp';
}
