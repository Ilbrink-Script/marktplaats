<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Cviebrock\EloquentSluggable\Sluggable;
use Malhal\Geographical\Geographical;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use Sluggable;
    use Geographical;
    use Billable {
        createOrGetStripeCustomer as traitCreateOrGetStripeCustomer;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'zipcode',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
        'is_admin',
        'is_seller',
        'distance_km',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * The user roles that belong to the user
     */
    public function userRoles()
    {
        return $this->belongsToMany(UserRole::class);
    }

    /**
     * The advertisements that belong to the user
     */
    public function advertisements()
    {
        return $this->hasMany(Advertisement::class);
    }

    protected function isUserRole($slug)
    {
        return $this->userRoles()->where('slug', $slug)->count() > 0;
    }

    public function getIsSellerAttribute()
    {
        return $this->isUserRole('seller');
    }

    public function getIsAdminAttribute()
    {
        return $this->isUserRole('admin');
    }

    public function messageThreads()
    {
        return $this->hasMany(MessageThread::class)->orderByDesc('created_at');
    }

    public function advertisementMessageThreads()
    {
        return $this->hasManyThrough(MessageThread::class, Advertisement::class)->orderByDesc('created_at');
    }

    /**
     * Return the distance in kilometers 
     *
     * @return 
     */
    public function getDistanceKmAttribute()
    {
        if (!is_double($this->distance))
            return '?';

        return round($this->distance * 1.609344 / 1000);
    }

    public function commissions()
    {
        return $this->hasManyThrough(Commission::class, Advertisement::class);
    }

    public function createOrGetStripeCustomer(array $options = [])
    {
        return $this->traitCreateOrGetStripeCustomer(
            array_merge_recursive([
                'metadata' => [
                    'mp_customer_id' => $this->id,
                ]
            ], $options)
        );
    }
}
