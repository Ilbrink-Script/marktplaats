<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    use Sluggable;


    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
