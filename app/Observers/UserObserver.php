<?php

namespace App\Observers;

use App\Models\Pp4;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserObserver
{
    /**
     * Handle the User "saving" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function saving(User $user)
    {
        // $postcode = DB::table('4pp')->where('postcode', substr($user->zipcode, 0, 4))->first();
        $postcode = Pp4::where('postcode', substr($user->zipcode, 0, 4))->first();

        if ($postcode) {
            $user->latitude = $postcode->latitude;
            $user->longitude = $postcode->longitude;
        }
    }
}
