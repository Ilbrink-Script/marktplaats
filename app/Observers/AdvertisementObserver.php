<?php

namespace App\Observers;

use App\Models\Advertisement;
use App\Models\Pp4;

class AdvertisementObserver
{

    /**
     * Handle the Advertisement "saving" event.
     *
     * @param  \App\Models\Advertisement  $advertisement
     * @return void
     */
    public function saving(Advertisement $advertisement)
    {
        // $postcode = DB::table('4pp')->where('postcode', substr($advertisement->zipcode, 0, 4))->first();
        $postcode = Pp4::where('postcode', substr($advertisement->zipcode, 0, 4))->first();

        if ($postcode) {
            $advertisement->latitude = $postcode->latitude;
            $advertisement->longitude = $postcode->longitude;
        }
    }
}
