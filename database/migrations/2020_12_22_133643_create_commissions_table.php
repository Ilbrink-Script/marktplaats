<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('advertisement_id')->constrained();
            $table->foreignId('offer_id')->constrained();
            $table->foreignId('transaction_id')->nullable()->constrained();
            $table->string('payment_intent_id')->nullable();
            $table->decimal('price', 8, 2)->default(0.0);
            $table->boolean('paid')->default(false);
            $table->timestamps();
            $table->timestamp('paid_at')->nullable();
            $table->softDeletes();

            $table->unique(['advertisement_id', 'offer_id']);
            $table->unique('payment_intent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commissions');
    }
}
