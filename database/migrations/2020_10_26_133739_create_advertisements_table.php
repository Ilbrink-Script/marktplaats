<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements', function (Blueprint $table) {
            $table->id();

            $table->foreignId('advertisement_status_id')->constrained();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('offer_id')->nullable(); // ->constrained();
            $table->foreignId('transaction_id')->nullable()->constrained();

            $table->string('title');
            $table->string('slug')->unique();
            $table->text('body');
            $table->decimal('price', 8, 2)->default(0.0);
            
            $table->char('zipcode', 6); // dutch 1234AB
            $table->double('longitude')->nullable();
            $table->double('latitude')->nullable();

            $table->boolean('premium')->default(false);
            $table->timestamp('offer_accepted_at')->nullable();
            $table->softDeletes('deleted_at', 0);
            $table->string('payment_intent_id')->nullable();
            $table->timestamp('premium_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisements');
    }
}
