<?php

namespace Database\Factories;

use App\Models\Advertisement;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class AdvertisementFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Advertisement::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $createdAt = $this->faker->dateTimeBetween('-3 years');

        return [
            'title' => $this->faker->sentence(rand(1, 5), true),
            'body' => $this->faker->paragraphs(rand(1, 4), true),
            'price' => $this->faker->randomNumber(4),
            'zipcode' => DB::table('4pp')->inRandomOrder()->first()->postcode . strtoupper($this->faker->lexify("??")),
            'premium' => $this->faker->numberBetween(0, 10) === 0, // 10% of the time
            'created_at' => $createdAt,
            'updated_at' => $this->faker->dateTimeBetween($createdAt, '+1 month'),

            'user_id' => User::whereHas('userRoles', function (Builder $query) {
                $query->where('slug', 'seller');
            })->get()->random()->id,
        ];
    }
}
