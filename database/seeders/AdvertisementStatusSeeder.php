<?php

namespace Database\Seeders;

use App\Models\AdvertisementStatus;
use Illuminate\Database\Seeder;

class AdvertisementStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['Draft', 'Active', 'Sold'];

        foreach ($statuses as $status) {
            AdvertisementStatus::updateOrCreate([
                'title' => $status
            ]);
        }
    }
}
