<?php

namespace Database\Seeders;

use App\Models\Advertisement;
use App\Models\Message;
use App\Models\MessageThread;
use App\Models\User;
use Illuminate\Database\Seeder;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        // Message Threads
        Advertisement::all()->each(function (Advertisement $advertisement) use ($users) {
            $threadUser = $users->filter(function ($user) use ($advertisement) {
                return $user->id !== $advertisement->user_id;
            })->random();

            MessageThread::factory()->has(Message::factory()
                ->count(2)
                ->state(function (array $attributes, MessageThread $messageThread) use ($users, $threadUser) {
                    return [
                        'message_thread_id' => $messageThread->id,
                        'user_id' => $users->filter(function ($u) use ($threadUser) {
                            return $u->id !== $threadUser->id;
                        })->random()->id
                    ];
                }))->create([
                'advertisement_id' => $advertisement->id,
                'user_id' => $threadUser->id,
            ]);
        });
    }
}
