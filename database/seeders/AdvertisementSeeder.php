<?php

namespace Database\Seeders;

use App\Models\Advertisement;
use App\Models\AdvertisementStatus;
use App\Models\Category;
use App\Models\Offer;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Seeder;

class AdvertisementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // sellers that are about to make offers
        $users = User::all();
        $active = AdvertisementStatus::where('slug', 'active')->get()->first();

        $advertisements = Advertisement::factory()->has(Offer::factory()->count(3)->state(function (array $attributes, Advertisement $advertisement) use ($users) {
            // set user to not be the advertiser
            return [
                'user_id' => $users->filter(function ($user) use ($advertisement) {
                    return $user->id != $advertisement->id;
                })->random()->id,
                'price' => rand(1, 1.1 * $advertisement->price),
            ];
        }))->count(35)->create([
            'advertisement_status_id' => $active->id,
            // 'user_id' => $sellers->random()->id,
        ]);

        // save
        $categories = Category::all();
        $advertisements->each(function ($advertisement) use ($categories) {
            $advertisement->categories()->attach($categories->random(rand(1, 5)));
        });
    }
}
