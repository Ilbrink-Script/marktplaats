<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserRole;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seller = UserRole::where('slug', 'seller')->first();

        // paul
        $paul = User::updateOrCreate([
            'name' => 'Paul',
            'email' => 'paul@example.com',
            'zipcode' => '9718LJ',
            'password' => bcrypt('jetstream'),
        ]);
        $paul->userRoles()->attach([$seller->id, UserRole::where('slug', 'admin')->first()->id]);

        // gertjan
        User::updateOrCreate([
            'name' => 'Gertjan',
            'email' => 'gertjan@example.com',
            'zipcode' => '7251RR',
            'password' => bcrypt('jetstream'),
        ])->userRoles()->attach($seller);

        // sellers
        $seller->users()->saveMany(User::factory()->count(5)->create());

        // normal users
        User::factory()->count(15)->create();
    }
}
